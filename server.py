import argparse

import flwr as fl
import numpy as np
from typing import List, Optional, Tuple
from collections import OrderedDict

import torch
import torch.nn as nn


min_available_clients = 8
fraction_fit = 0.5
fraction_eval = 1.0
n_input = 561
n_output = 6


class Network(nn.Module):
    def __init__(self):
        super().__init__()

        self.l1 = nn.Linear(n_input, 64)
        self.l2 = nn.Linear(64, 128)
        self.l3 = nn.Linear(128,64)
        self.output = nn.Linear(64, n_output)

        # Define sigmoid activation and softmax output
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.l1(x)
        x = self.relu(x)
        x = self.l2(x)
        x = self.relu(x)
        x = self.l3(x)
        x = self.relu(x)
        x = self.output(x)
        x = self.softmax(x)

        return x


class SaveModelStrategy(fl.server.strategy.FedAvg):
    def aggregate_fit(
        self,
        server_round: int,
        results: List[Tuple[fl.server.client_proxy.ClientProxy, fl.common.FitRes]],
        failures: List[BaseException],
    ) -> Optional[fl.common.Weights]:

        aggregated_parameters_tuple = super().aggregate_fit(server_round, results, failures)
        aggregated_parameters, _ = aggregated_parameters_tuple

        if server_round == args.n_rounds:
            # Save aggregated_weights
            print(f"Saving round {server_round} aggregated_weights...")
            # Convert `Parameters` to `List[np.ndarray]`
            aggregated_weights: List[np.ndarray] = fl.common.parameters_to_weights(aggregated_parameters)

            # Convert `List[np.ndarray]` to PyTorch`state_dict`
            params_dict = zip(net.state_dict().keys(), aggregated_weights)
            state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
            net.load_state_dict(state_dict, strict=True)
            torch.save(net.state_dict(), f"model_round_{server_round}.pth")

        return aggregated_parameters_tuple


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--n_rounds", type=int, required=False, default=50, help="number of training rounds"
    )
    args = parser.parse_args()

    net = Network().to("cpu")

    strategy = SaveModelStrategy(
        min_available_clients = min_available_clients,
        fraction_fit = fraction_fit,
        fraction_eval = fraction_eval
    )

    fl.server.start_server(
        server_address="[::]:8080",
        config={
            "num_rounds": args.n_rounds
        },
        strategy=strategy,
        force_final_distributed_eval=True
    )
