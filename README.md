# Supporting Efficient Workflow Deployment of Federated Learning Systems on the Computing Continuum

This repository contains artifacts (code, notebooks) of experiments conducted for this poster.

## Requirements (libraries + version used for experiments)
* pandas (1.4.1)
* numpy (1.22.3)
* scikit-learn (1.0.2)
* torch (1.11.0)
* flwr (0.18.0)
* tensorboard (2.9.1)
* matplotlib (3.5.1)
* seaborn (0.11.2)

## Dataset
The Human Activity Recognition dataset used for our experiments is provided in the [HAR Dataset](./HAR/) folder.
It was originally downloaded on the following website.
https://www.kaggle.com/datasets/uciml/human-activity-recognition-with-smartphones

## Artifacts
* [HAR Dataset](./HAR/)
* [requirements.txt](requirements.txt)
* Source files
  * [client.py](client.py) is the source code for launching a FL client
  * [server.py](server.py) is the source code for launching a FL server
  * [utils/dataset.py](utils/dataset.py) provide utility function for preprocessing the dataset
  * [start_clients.sh](start_clients.sh) is a script for launching several clients with different local datasets
* Notebooks
  * [impact_of_hyperparameter_tuning.ipynb](impact_of_hyperparameter_tuning.ipynb)
  * [impact_of_data_drift.ipynb](impact_of_data_drift.ipynb)
* [results](./results/) contains tensorboard log files from experiments conducted for this poster and a model trained with FL which we use for investigating the impact of data drift.

## How to reproduce experiments ?
I - Launch the server
```bash
python server.py --n_rounds [number_of_rounds]
```
II - Launch the clients with specific parameters
```bash
./start_clients.sh [number_of_clients_to_run] [client_learning_rate] [number_of_local_epochs]
```
