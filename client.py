from collections import OrderedDict
import argparse

import torch
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter

import flwr as fl

from utils.dataset import prepare_dataset


n_input = 561
n_output = 6
batchsize = 64


class Network(nn.Module):
    def __init__(self):
        super().__init__()

        self.l1 = nn.Linear(n_input, 64)
        self.l2 = nn.Linear(64, 128)
        self.l3 = nn.Linear(128,64)
        self.output = nn.Linear(64, n_output)

        # Define sigmoid activation and softmax output
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.l1(x)
        x = self.relu(x)
        x = self.l2(x)
        x = self.relu(x)
        x = self.l3(x)
        x = self.relu(x)
        x = self.output(x)
        x = self.softmax(x)

        return x


def train(net, trainloader, epochs, lr=0.001, momentum=0, device="cpu"):
    """Train the network on the training set."""
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(net.parameters(), momentum=momentum, lr=lr)
    for _ in range(epochs):
        for data, labels in trainloader:
            data, labels = data.detach().clone().float().to(device), labels.to(device)
            optimizer.zero_grad()
            loss = criterion(net(data), labels)
            loss.backward()
            optimizer.step()


def test(net, testloader, device):
    """Validate the network on the entire test set."""
    criterion = torch.nn.CrossEntropyLoss()
    correct, total, loss = 0, 0, 0.0
    with torch.no_grad():
        for data, labels in testloader:
            data, labels = data.detach().clone().float().to(device), labels.to(device)
            outputs = net(data)
            loss += criterion(outputs, labels).item()
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    accuracy = correct / total
    loss = loss / len(testloader)
    return loss, accuracy


class Client(fl.client.NumPyClient):
    def __init__(self, trainloader, testloader, writer, lr=0.001, momentum=0.0, epochs=1):
        self.lr = lr
        self.momentum = momentum
        self.trainloader = trainloader
        self.testloader = testloader
        self.writer = writer
        self.round = 0
        self.epochs=epochs

    # return the model weights as a list of NumPy ndarrays
    def get_parameters(self):
        return [val.cpu().numpy() for _, val in net.state_dict().items()]

    # update the local model weights with the parameters received from the
    # server (optional)
    def set_parameters(self, parameters):
        params_dict = zip(net.state_dict().keys(), parameters)
        state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
        net.load_state_dict(state_dict, strict=True)

    # set the local model weights
    # train the local model
    # receive the updated local model weights
    def fit(self, parameters, config):
        self.set_parameters(parameters)
        train(net, self.trainloader, epochs=self.epochs, lr=self.lr, momentum=self.momentum, device=DEVICE)
        return self.get_parameters(), len(self.trainloader.dataset), {}

    # test the local model
    def evaluate(self, parameters, config):
        self.set_parameters(parameters)
        loss, accuracy = test(net, self.testloader, device=DEVICE)
        self.round += 1
        self.writer.add_scalar(f"Accuracy/client_{args.num}", float(accuracy), self.round*self.epochs)
        return float(loss), len(self.testloader.dataset), {"accuracy": float(accuracy)}


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--num", type=int, required=False, default=0, help="client number"
    )
    parser.add_argument(
        "--lr", type=float, required=False, default=0.03, help="client learning rate"
    )
    parser.add_argument(
        "--local_epochs", type=int, required=False, default=1, help="local epochs"
    )
    parser.add_argument(
        "--momentum", type=float, required=False, default=0.9, help="momentum"
    )
    args = parser.parse_args()


    writer = SummaryWriter(log_dir=f"/tmp/app/runs/HAR/lr-{args.lr}-epochs-{args.local_epochs}/")
    writer.add_scalar("hp/lr", args.lr)
    writer.add_scalar("hp/momentum", args.momentum)
    writer.add_scalar("hp/local_epoch", args.local_epochs)

    DEVICE = "cpu"

    net = Network().to(DEVICE)
    trainloaders, testloaders, _ = prepare_dataset(batchsize)

    fl.client.start_numpy_client(
        server_address="[::]:8080",
        client=Client(
            trainloader=trainloaders[args.num],
            testloader=testloaders[args.num],
            writer=writer,
            lr=args.lr,
            momentum=args.momentum,
            epochs=args.local_epochs
        ),
        grpc_max_message_length=1073741824,
    )
