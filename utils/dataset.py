# Imports
import pandas

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder

from sklearn.model_selection import train_test_split

import torch

from torch.utils.data import DataLoader
from torch.utils.data import TensorDataset


seed = 42


def prepare_dataset(batchsize):
    df = pandas.concat([pandas.read_csv('./HAR/train.csv'), pandas.read_csv('./HAR/test.csv')])

    datasets = []
    targets = []
    for i in range(1,31):
        dataset = df.loc[df['subject'] == i]
        dataset = dataset.drop('subject', axis=1)
        target = dataset.pop('Activity')
        datasets.append(dataset)
        targets.append(target)

    # Preprocessing
    min_max_scaler = MinMaxScaler()
    label_encoder = LabelEncoder()

    for i in range(len(datasets)):
        datasets[i] = min_max_scaler.fit_transform(datasets[i])
        targets[i] = label_encoder.fit_transform(targets[i])

    # Train-test split
    data_train = []
    data_test = []
    target_train = []
    target_test = []

    for i in range(len(datasets)):
        d_train, d_test, t_train, t_test = train_test_split(
            datasets[i], targets[i], test_size=0.2, random_state=42
        )
        data_train.append(d_train)
        data_test.append(d_test)
        target_train.append(t_train)
        target_test.append(t_test)

    # Train and test loader
    trainloaders = []
    testloaders = []
    for i in range(len(data_train)):
        train = TensorDataset(torch.from_numpy(data_train[i]), torch.from_numpy(target_train[i]))
        test = TensorDataset(torch.from_numpy(data_test[i]), torch.from_numpy(target_test[i]))
        trainloaders.append(DataLoader(train, batch_size=batchsize))
        testloaders.append(DataLoader(test, batch_size=batchsize))

    return trainloaders, testloaders, label_encoder.classes_
